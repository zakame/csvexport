use strict;
use warnings;
use utf8;
use t::Util;
use Plack::Test;
use Plack::Util;
use Test::More;
use Test::Requires 'Test::WWW::Mechanize::PSGI';

my $app = Plack::Util::load_psgi 'script/csvexport-server';

my $mech = Test::WWW::Mechanize::PSGI->new(app => $app);
$mech->get_ok('/');

$mech->get_ok('/rows.csv?max=10');
$mech->get('/rows.csv?max=invalid');
is( $mech->status, 404, 'invalid max' );

done_testing;
