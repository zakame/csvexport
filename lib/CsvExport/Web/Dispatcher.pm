package CsvExport::Web::Dispatcher;
use strict;
use warnings;
use utf8;
use Amon2::Web::Dispatcher::RouterBoom;

use String::Random;
use Text::CSV;

sub gen_rows {
    my $MAX = shift;
    return sub {
        my $respond = shift;
        my $writer = $respond->( [ 200, [ 'Content-Type' => 'text/csv' ] ] );

        my $csv = Text::CSV->new( { binary => 1 } )
            or die "Cannot use CSV: ", Text::CSV->error_diag;

        for my $row ( 1 .. $MAX ) {
            $csv->combine(
                $row,
                String::Random->new->randpattern('...'),
                String::Random->new->randpattern('s!s')
            );
            $writer->write( $csv->string . "\n" );
        }
        $writer->close;
        }
}

get '/rows.csv' => sub {
    my ($c) = @_;
    my $max = $c->req->parameters->{max} || 500;

    return $c->res_404 unless $max =~ /\d+/;
    return $c->streaming( gen_rows($max) );
};

any '/' => sub {
    my ($c) = @_;
    return $c->render('index.tx');
};

1;
